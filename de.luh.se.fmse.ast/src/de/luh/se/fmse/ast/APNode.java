/**
 */
package de.luh.se.fmse.ast;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AP Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.luh.se.fmse.ast.APNode#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.luh.se.fmse.ast.AstPackage#getAPNode()
 * @model
 * @generated
 */
public interface APNode extends Node {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see de.luh.se.fmse.ast.AstPackage#getAPNode_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.luh.se.fmse.ast.APNode#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // APNode
