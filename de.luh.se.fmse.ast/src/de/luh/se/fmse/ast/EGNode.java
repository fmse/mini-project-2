/**
 */
package de.luh.se.fmse.ast;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EG Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.luh.se.fmse.ast.EGNode#getChild <em>Child</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.luh.se.fmse.ast.AstPackage#getEGNode()
 * @model
 * @generated
 */
public interface EGNode extends Node {
	/**
	 * Returns the value of the '<em><b>Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Child</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child</em>' containment reference.
	 * @see #setChild(Node)
	 * @see de.luh.se.fmse.ast.AstPackage#getEGNode_Child()
	 * @model containment="true"
	 * @generated
	 */
	Node getChild();

	/**
	 * Sets the value of the '{@link de.luh.se.fmse.ast.EGNode#getChild <em>Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Child</em>' containment reference.
	 * @see #getChild()
	 * @generated
	 */
	void setChild(Node value);

} // EGNode
