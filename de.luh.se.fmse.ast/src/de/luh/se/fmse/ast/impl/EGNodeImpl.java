/**
 */
package de.luh.se.fmse.ast.impl;

import java.util.ArrayList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.luh.se.fmse.ast.AstPackage;
import de.luh.se.fmse.ast.EGNode;
import de.luh.se.fmse.ast.Node;
import de.luh.se.fmse.lts.LTS;
import de.luh.se.fmse.lts.State;
import de.luh.se.fmse.lts.Transition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EG Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.luh.se.fmse.ast.impl.EGNodeImpl#getChild <em>Child</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EGNodeImpl extends NodeImpl implements EGNode {
	/**
	 * The cached value of the '{@link #getChild() <em>Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChild()
	 * @generated
	 * @ordered
	 */
	protected Node child;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EGNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AstPackage.eINSTANCE.getEGNode();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getChild() {
		return child;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChild(Node newChild, NotificationChain msgs) {
		Node oldChild = child;
		child = newChild;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AstPackage.EG_NODE__CHILD, oldChild, newChild);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChild(Node newChild) {
		if (newChild != child) {
			NotificationChain msgs = null;
			if (child != null)
				msgs = ((InternalEObject)child).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AstPackage.EG_NODE__CHILD, null, msgs);
			if (newChild != null)
				msgs = ((InternalEObject)newChild).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AstPackage.EG_NODE__CHILD, null, msgs);
			msgs = basicSetChild(newChild, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AstPackage.EG_NODE__CHILD, newChild, newChild));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AstPackage.EG_NODE__CHILD:
				return basicSetChild(null, msgs);
		}
		return eDynamicInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AstPackage.EG_NODE__CHILD:
				return getChild();
		}
		return eDynamicGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AstPackage.EG_NODE__CHILD:
				setChild((Node)newValue);
				return;
		}
		eDynamicSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AstPackage.EG_NODE__CHILD:
				setChild((Node)null);
				return;
		}
		eDynamicUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AstPackage.EG_NODE__CHILD:
				return child != null;
		}
		return eDynamicIsSet(featureID);
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public EList<State> validate(LTS lts) {
		EList<State> resultStates = new UniqueEList<State>();
		
		if (this.child == null)
			return resultStates;
		
		//childStates contains all states that are true for the given (AP), like EG(AP)
		EList<State> childStates = this.child.validate(lts);
		System.out.println("ChildStates:");
		for (State s : childStates) {
			System.out.println(s.getName());
		}
		System.out.println("---------------");
		ArrayList<ArrayList<State>> scc = findSCCs(childStates);
		
		for (ArrayList<State> cycle : scc) { //in each cycle
			System.out.println("Zirkel:");
			for (State state : cycle) { //look at each state
				System.out.print(state.getName());
				resultStates = getStatesWithPath(childStates, state, new BasicEList<State>());
			}
		}
		System.out.println("---------------");
		for (State s : resultStates) {
			System.out.println(s.getName());
		}
		
		
		//store in each state the satisfied formula
		for (State s : resultStates)
			s.getSatisfiedNodes().add(this.toString());
		
		return resultStates;
	}
	
	/**
	 * @generated NOT
	 */
	public ArrayList<ArrayList<State>> findSCCs(EList<State> statelist) {
		ArrayList<ArrayList<State>> scc = new ArrayList<ArrayList<State>>();
		
		//for sure duplicates in this, to lazy now
		for (State state : statelist) {
			scc.addAll(makeDFS(new ArrayList<State>(), state, statelist));
		}
		
		return scc;
	}
	
	/**
	 *@generated NOT 
	 */
	protected ArrayList<ArrayList<State>> makeDFS(ArrayList<State> statesSoFar, State currentState, EList<State> allowedStates) {
		ArrayList<ArrayList<State>> cycle = new ArrayList<ArrayList<State>>();
		statesSoFar.add(currentState);
		
		for (Transition trans : currentState.getOutgoingTransitions()) {
			if (!allowedStates.contains(trans.getToState()))
				continue;
			if (statesSoFar.contains(trans.getToState())) {
				ArrayList<State> result = new ArrayList<State>();
				result.addAll(statesSoFar);
				result.add(trans.getToState());
				cycle.add(result);
				break;
			}
			
			cycle.addAll(makeDFS(statesSoFar, trans.getToState(), allowedStates));
		}
		
		return cycle;
		
	}
	
	/**
	 *@generated NOT 
	 */
	protected EList<State> getStatesWithPath(EList<State> allowedStates, State target, EList<State> processedStates) {
		EList<State> resultStates = new UniqueEList<State>();
		
		for (Transition trans : target.getIncomingTransitions()) {
			if (processedStates.contains(trans.getFromState()) || !allowedStates.contains(trans.getToState()))
				continue;
			
			processedStates.add(trans.getFromState());
			
			if (allowedStates.contains(trans.getFromState()))
				resultStates.add(trans.getFromState());
			
			resultStates.addAll(getStatesWithPath(allowedStates, trans.getFromState(), processedStates));
		}
		
		return resultStates;
	}
	
	/**
	 * @generated NOT
	 */
	public String toString() {
		return "EG(" + this.child.toString() + ")";
	}

} //EGNodeImpl
