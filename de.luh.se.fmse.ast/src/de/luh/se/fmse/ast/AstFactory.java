/**
 */
package de.luh.se.fmse.ast;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.luh.se.fmse.ast.AstPackage
 * @generated
 */
public interface AstFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AstFactory eINSTANCE = de.luh.se.fmse.ast.impl.AstFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>EU Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EU Node</em>'.
	 * @generated
	 */
	EUNode createEUNode();

	/**
	 * Returns a new object of class '<em>EX Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EX Node</em>'.
	 * @generated
	 */
	EXNode createEXNode();

	/**
	 * Returns a new object of class '<em>EG Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EG Node</em>'.
	 * @generated
	 */
	EGNode createEGNode();

	/**
	 * Returns a new object of class '<em>AND Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>AND Node</em>'.
	 * @generated
	 */
	ANDNode createANDNode();

	/**
	 * Returns a new object of class '<em>OR Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OR Node</em>'.
	 * @generated
	 */
	ORNode createORNode();

	/**
	 * Returns a new object of class '<em>NOT Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>NOT Node</em>'.
	 * @generated
	 */
	NOTNode createNOTNode();

	/**
	 * Returns a new object of class '<em>AP Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>AP Node</em>'.
	 * @generated
	 */
	APNode createAPNode();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	AstPackage getAstPackage();

} //AstFactory
