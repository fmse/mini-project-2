/**
 */
package de.luh.se.fmse.ast;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.luh.se.fmse.ast.AstFactory
 * @model kind="package"
 * @generated
 */
public interface AstPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ast";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de.luh.se.fmse.ast/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ast";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AstPackage eINSTANCE = de.luh.se.fmse.ast.impl.AstPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.ast.impl.NodeImpl <em>Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.ast.impl.NodeImpl
	 * @see de.luh.se.fmse.ast.impl.AstPackageImpl#getNode()
	 * @generated
	 */
	int NODE = 0;

	/**
	 * The number of structural features of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Validate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE___VALIDATE__LTS = 0;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE___TO_STRING = 1;

	/**
	 * The number of operations of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.ast.impl.EUNodeImpl <em>EU Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.ast.impl.EUNodeImpl
	 * @see de.luh.se.fmse.ast.impl.AstPackageImpl#getEUNode()
	 * @generated
	 */
	int EU_NODE = 1;

	/**
	 * The feature id for the '<em><b>Left Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EU_NODE__LEFT_CHILD = NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EU_NODE__RIGHT_CHILD = NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>EU Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EU_NODE_FEATURE_COUNT = NODE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Validate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EU_NODE___VALIDATE__LTS = NODE___VALIDATE__LTS;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EU_NODE___TO_STRING = NODE___TO_STRING;

	/**
	 * The number of operations of the '<em>EU Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EU_NODE_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.ast.impl.EXNodeImpl <em>EX Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.ast.impl.EXNodeImpl
	 * @see de.luh.se.fmse.ast.impl.AstPackageImpl#getEXNode()
	 * @generated
	 */
	int EX_NODE = 2;

	/**
	 * The feature id for the '<em><b>Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EX_NODE__CHILD = NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>EX Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EX_NODE_FEATURE_COUNT = NODE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Validate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EX_NODE___VALIDATE__LTS = NODE___VALIDATE__LTS;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EX_NODE___TO_STRING = NODE___TO_STRING;

	/**
	 * The number of operations of the '<em>EX Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EX_NODE_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.ast.impl.EGNodeImpl <em>EG Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.ast.impl.EGNodeImpl
	 * @see de.luh.se.fmse.ast.impl.AstPackageImpl#getEGNode()
	 * @generated
	 */
	int EG_NODE = 3;

	/**
	 * The feature id for the '<em><b>Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EG_NODE__CHILD = NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>EG Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EG_NODE_FEATURE_COUNT = NODE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Validate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EG_NODE___VALIDATE__LTS = NODE___VALIDATE__LTS;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EG_NODE___TO_STRING = NODE___TO_STRING;

	/**
	 * The number of operations of the '<em>EG Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EG_NODE_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.ast.impl.ANDNodeImpl <em>AND Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.ast.impl.ANDNodeImpl
	 * @see de.luh.se.fmse.ast.impl.AstPackageImpl#getANDNode()
	 * @generated
	 */
	int AND_NODE = 4;

	/**
	 * The feature id for the '<em><b>Left Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_NODE__LEFT_CHILD = NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_NODE__RIGHT_CHILD = NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>AND Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_NODE_FEATURE_COUNT = NODE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Validate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_NODE___VALIDATE__LTS = NODE___VALIDATE__LTS;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_NODE___TO_STRING = NODE___TO_STRING;

	/**
	 * The number of operations of the '<em>AND Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_NODE_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.ast.impl.ORNodeImpl <em>OR Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.ast.impl.ORNodeImpl
	 * @see de.luh.se.fmse.ast.impl.AstPackageImpl#getORNode()
	 * @generated
	 */
	int OR_NODE = 5;

	/**
	 * The feature id for the '<em><b>Left Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_NODE__LEFT_CHILD = NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_NODE__RIGHT_CHILD = NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>OR Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_NODE_FEATURE_COUNT = NODE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Validate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_NODE___VALIDATE__LTS = NODE___VALIDATE__LTS;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_NODE___TO_STRING = NODE___TO_STRING;

	/**
	 * The number of operations of the '<em>OR Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_NODE_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.ast.impl.NOTNodeImpl <em>NOT Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.ast.impl.NOTNodeImpl
	 * @see de.luh.se.fmse.ast.impl.AstPackageImpl#getNOTNode()
	 * @generated
	 */
	int NOT_NODE = 6;

	/**
	 * The feature id for the '<em><b>Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_NODE__CHILD = NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>NOT Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_NODE_FEATURE_COUNT = NODE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Validate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_NODE___VALIDATE__LTS = NODE___VALIDATE__LTS;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_NODE___TO_STRING = NODE___TO_STRING;

	/**
	 * The number of operations of the '<em>NOT Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_NODE_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link de.luh.se.fmse.ast.impl.APNodeImpl <em>AP Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.ast.impl.APNodeImpl
	 * @see de.luh.se.fmse.ast.impl.AstPackageImpl#getAPNode()
	 * @generated
	 */
	int AP_NODE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AP_NODE__NAME = NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>AP Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AP_NODE_FEATURE_COUNT = NODE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Validate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AP_NODE___VALIDATE__LTS = NODE___VALIDATE__LTS;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AP_NODE___TO_STRING = NODE___TO_STRING;

	/**
	 * The number of operations of the '<em>AP Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AP_NODE_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.ast.Node <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node</em>'.
	 * @see de.luh.se.fmse.ast.Node
	 * @generated
	 */
	EClass getNode();

	/**
	 * Returns the meta object for the '{@link de.luh.se.fmse.ast.Node#validate(de.luh.se.fmse.lts.LTS) <em>Validate</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Validate</em>' operation.
	 * @see de.luh.se.fmse.ast.Node#validate(de.luh.se.fmse.lts.LTS)
	 * @generated
	 */
	EOperation getNode__Validate__LTS();

	/**
	 * Returns the meta object for the '{@link de.luh.se.fmse.ast.Node#toString() <em>To String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>To String</em>' operation.
	 * @see de.luh.se.fmse.ast.Node#toString()
	 * @generated
	 */
	EOperation getNode__ToString();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.ast.EUNode <em>EU Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EU Node</em>'.
	 * @see de.luh.se.fmse.ast.EUNode
	 * @generated
	 */
	EClass getEUNode();

	/**
	 * Returns the meta object for the containment reference '{@link de.luh.se.fmse.ast.EUNode#getLeftChild <em>Left Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Child</em>'.
	 * @see de.luh.se.fmse.ast.EUNode#getLeftChild()
	 * @see #getEUNode()
	 * @generated
	 */
	EReference getEUNode_LeftChild();

	/**
	 * Returns the meta object for the containment reference '{@link de.luh.se.fmse.ast.EUNode#getRightChild <em>Right Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Child</em>'.
	 * @see de.luh.se.fmse.ast.EUNode#getRightChild()
	 * @see #getEUNode()
	 * @generated
	 */
	EReference getEUNode_RightChild();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.ast.EXNode <em>EX Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EX Node</em>'.
	 * @see de.luh.se.fmse.ast.EXNode
	 * @generated
	 */
	EClass getEXNode();

	/**
	 * Returns the meta object for the containment reference '{@link de.luh.se.fmse.ast.EXNode#getChild <em>Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Child</em>'.
	 * @see de.luh.se.fmse.ast.EXNode#getChild()
	 * @see #getEXNode()
	 * @generated
	 */
	EReference getEXNode_Child();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.ast.EGNode <em>EG Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EG Node</em>'.
	 * @see de.luh.se.fmse.ast.EGNode
	 * @generated
	 */
	EClass getEGNode();

	/**
	 * Returns the meta object for the containment reference '{@link de.luh.se.fmse.ast.EGNode#getChild <em>Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Child</em>'.
	 * @see de.luh.se.fmse.ast.EGNode#getChild()
	 * @see #getEGNode()
	 * @generated
	 */
	EReference getEGNode_Child();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.ast.ANDNode <em>AND Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AND Node</em>'.
	 * @see de.luh.se.fmse.ast.ANDNode
	 * @generated
	 */
	EClass getANDNode();

	/**
	 * Returns the meta object for the containment reference '{@link de.luh.se.fmse.ast.ANDNode#getLeftChild <em>Left Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Child</em>'.
	 * @see de.luh.se.fmse.ast.ANDNode#getLeftChild()
	 * @see #getANDNode()
	 * @generated
	 */
	EReference getANDNode_LeftChild();

	/**
	 * Returns the meta object for the containment reference '{@link de.luh.se.fmse.ast.ANDNode#getRightChild <em>Right Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Child</em>'.
	 * @see de.luh.se.fmse.ast.ANDNode#getRightChild()
	 * @see #getANDNode()
	 * @generated
	 */
	EReference getANDNode_RightChild();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.ast.ORNode <em>OR Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OR Node</em>'.
	 * @see de.luh.se.fmse.ast.ORNode
	 * @generated
	 */
	EClass getORNode();

	/**
	 * Returns the meta object for the containment reference '{@link de.luh.se.fmse.ast.ORNode#getLeftChild <em>Left Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Child</em>'.
	 * @see de.luh.se.fmse.ast.ORNode#getLeftChild()
	 * @see #getORNode()
	 * @generated
	 */
	EReference getORNode_LeftChild();

	/**
	 * Returns the meta object for the containment reference '{@link de.luh.se.fmse.ast.ORNode#getRightChild <em>Right Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Child</em>'.
	 * @see de.luh.se.fmse.ast.ORNode#getRightChild()
	 * @see #getORNode()
	 * @generated
	 */
	EReference getORNode_RightChild();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.ast.NOTNode <em>NOT Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NOT Node</em>'.
	 * @see de.luh.se.fmse.ast.NOTNode
	 * @generated
	 */
	EClass getNOTNode();

	/**
	 * Returns the meta object for the containment reference '{@link de.luh.se.fmse.ast.NOTNode#getChild <em>Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Child</em>'.
	 * @see de.luh.se.fmse.ast.NOTNode#getChild()
	 * @see #getNOTNode()
	 * @generated
	 */
	EReference getNOTNode_Child();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.ast.APNode <em>AP Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AP Node</em>'.
	 * @see de.luh.se.fmse.ast.APNode
	 * @generated
	 */
	EClass getAPNode();

	/**
	 * Returns the meta object for the attribute '{@link de.luh.se.fmse.ast.APNode#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.luh.se.fmse.ast.APNode#getName()
	 * @see #getAPNode()
	 * @generated
	 */
	EAttribute getAPNode_Name();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AstFactory getAstFactory();

} //AstPackage
