/**
 */
package de.luh.se.fmse.lts.impl;

import de.luh.se.fmse.lts.Alphabet;
import de.luh.se.fmse.lts.LTS;
import de.luh.se.fmse.lts.LtsPackage;
import de.luh.se.fmse.lts.State;
import de.luh.se.fmse.lts.Transition;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LTS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.luh.se.fmse.lts.impl.LTSImpl#getStates <em>States</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.LTSImpl#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.LTSImpl#getAlphabet <em>Alphabet</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.LTSImpl#getStartState <em>Start State</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LTSImpl extends MinimalEObjectImpl.Container implements LTS {
	/**
	 * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStates()
	 * @generated
	 * @ordered
	 */
	protected EList<State> states;

	/**
	 * The cached value of the '{@link #getTransitions() <em>Transitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> transitions;

	/**
	 * The cached value of the '{@link #getAlphabet() <em>Alphabet</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlphabet()
	 * @generated
	 * @ordered
	 */
	protected Alphabet alphabet;

	/**
	 * The cached value of the '{@link #getStartState() <em>Start State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartState()
	 * @generated
	 * @ordered
	 */
	protected State startState;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LTSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LtsPackage.eINSTANCE.getLTS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getStates() {
		if (states == null) {
			states = new EObjectContainmentEList<State>(State.class, this, LtsPackage.LTS__STATES);
		}
		return states;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getTransitions() {
		if (transitions == null) {
			transitions = new EObjectContainmentEList<Transition>(Transition.class, this, LtsPackage.LTS__TRANSITIONS);
		}
		return transitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Alphabet getAlphabet() {
		return alphabet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlphabet(Alphabet newAlphabet, NotificationChain msgs) {
		Alphabet oldAlphabet = alphabet;
		alphabet = newAlphabet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LtsPackage.LTS__ALPHABET, oldAlphabet, newAlphabet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlphabet(Alphabet newAlphabet) {
		if (newAlphabet != alphabet) {
			NotificationChain msgs = null;
			if (alphabet != null)
				msgs = ((InternalEObject)alphabet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LtsPackage.LTS__ALPHABET, null, msgs);
			if (newAlphabet != null)
				msgs = ((InternalEObject)newAlphabet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LtsPackage.LTS__ALPHABET, null, msgs);
			msgs = basicSetAlphabet(newAlphabet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.LTS__ALPHABET, newAlphabet, newAlphabet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getStartState() {
		if (startState != null && startState.eIsProxy()) {
			InternalEObject oldStartState = (InternalEObject)startState;
			startState = (State)eResolveProxy(oldStartState);
			if (startState != oldStartState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LtsPackage.LTS__START_STATE, oldStartState, startState));
			}
		}
		return startState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetStartState() {
		return startState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartState(State newStartState) {
		State oldStartState = startState;
		startState = newStartState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.LTS__START_STATE, oldStartState, startState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LtsPackage.LTS__STATES:
				return ((InternalEList<?>)getStates()).basicRemove(otherEnd, msgs);
			case LtsPackage.LTS__TRANSITIONS:
				return ((InternalEList<?>)getTransitions()).basicRemove(otherEnd, msgs);
			case LtsPackage.LTS__ALPHABET:
				return basicSetAlphabet(null, msgs);
		}
		return eDynamicInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LtsPackage.LTS__STATES:
				return getStates();
			case LtsPackage.LTS__TRANSITIONS:
				return getTransitions();
			case LtsPackage.LTS__ALPHABET:
				return getAlphabet();
			case LtsPackage.LTS__START_STATE:
				if (resolve) return getStartState();
				return basicGetStartState();
		}
		return eDynamicGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LtsPackage.LTS__STATES:
				getStates().clear();
				getStates().addAll((Collection<? extends State>)newValue);
				return;
			case LtsPackage.LTS__TRANSITIONS:
				getTransitions().clear();
				getTransitions().addAll((Collection<? extends Transition>)newValue);
				return;
			case LtsPackage.LTS__ALPHABET:
				setAlphabet((Alphabet)newValue);
				return;
			case LtsPackage.LTS__START_STATE:
				setStartState((State)newValue);
				return;
		}
		eDynamicSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LtsPackage.LTS__STATES:
				getStates().clear();
				return;
			case LtsPackage.LTS__TRANSITIONS:
				getTransitions().clear();
				return;
			case LtsPackage.LTS__ALPHABET:
				setAlphabet((Alphabet)null);
				return;
			case LtsPackage.LTS__START_STATE:
				setStartState((State)null);
				return;
		}
		eDynamicUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LtsPackage.LTS__STATES:
				return states != null && !states.isEmpty();
			case LtsPackage.LTS__TRANSITIONS:
				return transitions != null && !transitions.isEmpty();
			case LtsPackage.LTS__ALPHABET:
				return alphabet != null;
			case LtsPackage.LTS__START_STATE:
				return startState != null;
		}
		return eDynamicIsSet(featureID);
	}

} //LTSImpl
