/**
 */
package de.luh.se.fmse.lts.impl;

import de.luh.se.fmse.lts.LtsPackage;
import de.luh.se.fmse.lts.State;
import de.luh.se.fmse.lts.Transition;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.luh.se.fmse.lts.impl.StateImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.StateImpl#getIncomingTransitions <em>Incoming Transitions</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.StateImpl#getOutgoingTransitions <em>Outgoing Transitions</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.StateImpl#getAtomicPrep <em>Atomic Prep</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.StateImpl#getSatisfiedNodes <em>Satisfied Nodes</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.StateImpl#isSatisfied <em>Satisfied</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.StateImpl#isLTSsatisfied <em>LT Ssatisfied</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StateImpl extends MinimalEObjectImpl.Container implements State {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIncomingTransitions() <em>Incoming Transitions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncomingTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> incomingTransitions;

	/**
	 * The cached value of the '{@link #getOutgoingTransitions() <em>Outgoing Transitions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutgoingTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> outgoingTransitions;

	/**
	 * The cached value of the '{@link #getAtomicPrep() <em>Atomic Prep</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicPrep()
	 * @generated
	 * @ordered
	 */
	protected EList<String> atomicPrep;

	/**
	 * The cached value of the '{@link #getSatisfiedNodes() <em>Satisfied Nodes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSatisfiedNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<String> satisfiedNodes;

	/**
	 * The default value of the '{@link #isSatisfied() <em>Satisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSatisfied()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SATISFIED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSatisfied() <em>Satisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSatisfied()
	 * @generated
	 * @ordered
	 */
	protected boolean satisfied = SATISFIED_EDEFAULT;

	/**
	 * The default value of the '{@link #isLTSsatisfied() <em>LT Ssatisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLTSsatisfied()
	 * @generated
	 * @ordered
	 */
	protected static final boolean LT_SSATISFIED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isLTSsatisfied() <em>LT Ssatisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLTSsatisfied()
	 * @generated
	 * @ordered
	 */
	protected boolean ltSsatisfied = LT_SSATISFIED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LtsPackage.eINSTANCE.getState();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.STATE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getIncomingTransitions() {
		if (incomingTransitions == null) {
			incomingTransitions = new EObjectResolvingEList<Transition>(Transition.class, this, LtsPackage.STATE__INCOMING_TRANSITIONS);
		}
		return incomingTransitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getOutgoingTransitions() {
		if (outgoingTransitions == null) {
			outgoingTransitions = new EObjectResolvingEList<Transition>(Transition.class, this, LtsPackage.STATE__OUTGOING_TRANSITIONS);
		}
		return outgoingTransitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getAtomicPrep() {
		if (atomicPrep == null) {
			atomicPrep = new EDataTypeUniqueEList<String>(String.class, this, LtsPackage.STATE__ATOMIC_PREP);
		}
		return atomicPrep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getSatisfiedNodes() {
		if (satisfiedNodes == null) {
			satisfiedNodes = new EDataTypeUniqueEList<String>(String.class, this, LtsPackage.STATE__SATISFIED_NODES);
		}
		return satisfiedNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSatisfied() {
		return satisfied;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSatisfied(boolean newSatisfied) {
		boolean oldSatisfied = satisfied;
		satisfied = newSatisfied;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.STATE__SATISFIED, oldSatisfied, satisfied));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isLTSsatisfied() {
		return ltSsatisfied;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLTSsatisfied(boolean newLTSsatisfied) {
		boolean oldLTSsatisfied = ltSsatisfied;
		ltSsatisfied = newLTSsatisfied;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.STATE__LT_SSATISFIED, oldLTSsatisfied, ltSsatisfied));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LtsPackage.STATE__NAME:
				return getName();
			case LtsPackage.STATE__INCOMING_TRANSITIONS:
				return getIncomingTransitions();
			case LtsPackage.STATE__OUTGOING_TRANSITIONS:
				return getOutgoingTransitions();
			case LtsPackage.STATE__ATOMIC_PREP:
				return getAtomicPrep();
			case LtsPackage.STATE__SATISFIED_NODES:
				return getSatisfiedNodes();
			case LtsPackage.STATE__SATISFIED:
				return isSatisfied();
			case LtsPackage.STATE__LT_SSATISFIED:
				return isLTSsatisfied();
		}
		return eDynamicGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LtsPackage.STATE__NAME:
				setName((String)newValue);
				return;
			case LtsPackage.STATE__INCOMING_TRANSITIONS:
				getIncomingTransitions().clear();
				getIncomingTransitions().addAll((Collection<? extends Transition>)newValue);
				return;
			case LtsPackage.STATE__OUTGOING_TRANSITIONS:
				getOutgoingTransitions().clear();
				getOutgoingTransitions().addAll((Collection<? extends Transition>)newValue);
				return;
			case LtsPackage.STATE__ATOMIC_PREP:
				getAtomicPrep().clear();
				getAtomicPrep().addAll((Collection<? extends String>)newValue);
				return;
			case LtsPackage.STATE__SATISFIED_NODES:
				getSatisfiedNodes().clear();
				getSatisfiedNodes().addAll((Collection<? extends String>)newValue);
				return;
			case LtsPackage.STATE__SATISFIED:
				setSatisfied((Boolean)newValue);
				return;
			case LtsPackage.STATE__LT_SSATISFIED:
				setLTSsatisfied((Boolean)newValue);
				return;
		}
		eDynamicSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LtsPackage.STATE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case LtsPackage.STATE__INCOMING_TRANSITIONS:
				getIncomingTransitions().clear();
				return;
			case LtsPackage.STATE__OUTGOING_TRANSITIONS:
				getOutgoingTransitions().clear();
				return;
			case LtsPackage.STATE__ATOMIC_PREP:
				getAtomicPrep().clear();
				return;
			case LtsPackage.STATE__SATISFIED_NODES:
				getSatisfiedNodes().clear();
				return;
			case LtsPackage.STATE__SATISFIED:
				setSatisfied(SATISFIED_EDEFAULT);
				return;
			case LtsPackage.STATE__LT_SSATISFIED:
				setLTSsatisfied(LT_SSATISFIED_EDEFAULT);
				return;
		}
		eDynamicUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LtsPackage.STATE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case LtsPackage.STATE__INCOMING_TRANSITIONS:
				return incomingTransitions != null && !incomingTransitions.isEmpty();
			case LtsPackage.STATE__OUTGOING_TRANSITIONS:
				return outgoingTransitions != null && !outgoingTransitions.isEmpty();
			case LtsPackage.STATE__ATOMIC_PREP:
				return atomicPrep != null && !atomicPrep.isEmpty();
			case LtsPackage.STATE__SATISFIED_NODES:
				return satisfiedNodes != null && !satisfiedNodes.isEmpty();
			case LtsPackage.STATE__SATISFIED:
				return satisfied != SATISFIED_EDEFAULT;
			case LtsPackage.STATE__LT_SSATISFIED:
				return ltSsatisfied != LT_SSATISFIED_EDEFAULT;
		}
		return eDynamicIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", atomicPrep: ");
		result.append(atomicPrep);
		result.append(", satisfiedNodes: ");
		result.append(satisfiedNodes);
		result.append(", satisfied: ");
		result.append(satisfied);
		result.append(", LTSsatisfied: ");
		result.append(ltSsatisfied);
		result.append(')');
		return result.toString();
	}

} //StateImpl
