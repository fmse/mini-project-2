/**
 */
package de.luh.se.fmse.lts;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.luh.se.fmse.lts.LtsFactory
 * @model kind="package"
 * @generated
 */
public interface LtsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "lts";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de.luh.se.fmse.lts/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "lts";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LtsPackage eINSTANCE = de.luh.se.fmse.lts.impl.LtsPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.lts.impl.LTSImpl <em>LTS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.lts.impl.LTSImpl
	 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getLTS()
	 * @generated
	 */
	int LTS = 0;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS__STATES = 0;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS__TRANSITIONS = 1;

	/**
	 * The feature id for the '<em><b>Alphabet</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS__ALPHABET = 2;

	/**
	 * The feature id for the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS__START_STATE = 3;

	/**
	 * The number of structural features of the '<em>LTS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.lts.impl.StateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.lts.impl.StateImpl
	 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getState()
	 * @generated
	 */
	int STATE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Incoming Transitions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__INCOMING_TRANSITIONS = 1;

	/**
	 * The feature id for the '<em><b>Outgoing Transitions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__OUTGOING_TRANSITIONS = 2;

	/**
	 * The feature id for the '<em><b>Atomic Prep</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ATOMIC_PREP = 3;

	/**
	 * The feature id for the '<em><b>Satisfied Nodes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__SATISFIED_NODES = 4;

	/**
	 * The feature id for the '<em><b>Satisfied</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__SATISFIED = 5;

	/**
	 * The feature id for the '<em><b>LT Ssatisfied</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__LT_SSATISFIED = 6;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.lts.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.lts.impl.TransitionImpl
	 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__NAME = 0;

	/**
	 * The feature id for the '<em><b>From State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__FROM_STATE = 1;

	/**
	 * The feature id for the '<em><b>To State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TO_STATE = 2;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.lts.impl.AlphabetImpl <em>Alphabet</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.lts.impl.AlphabetImpl
	 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getAlphabet()
	 * @generated
	 */
	int ALPHABET = 3;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALPHABET__ELEMENTS = 0;

	/**
	 * The number of structural features of the '<em>Alphabet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALPHABET_FEATURE_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.lts.LTS <em>LTS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LTS</em>'.
	 * @see de.luh.se.fmse.lts.LTS
	 * @generated
	 */
	EClass getLTS();

	/**
	 * Returns the meta object for the containment reference list '{@link de.luh.se.fmse.lts.LTS#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>States</em>'.
	 * @see de.luh.se.fmse.lts.LTS#getStates()
	 * @see #getLTS()
	 * @generated
	 */
	EReference getLTS_States();

	/**
	 * Returns the meta object for the containment reference list '{@link de.luh.se.fmse.lts.LTS#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transitions</em>'.
	 * @see de.luh.se.fmse.lts.LTS#getTransitions()
	 * @see #getLTS()
	 * @generated
	 */
	EReference getLTS_Transitions();

	/**
	 * Returns the meta object for the containment reference '{@link de.luh.se.fmse.lts.LTS#getAlphabet <em>Alphabet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Alphabet</em>'.
	 * @see de.luh.se.fmse.lts.LTS#getAlphabet()
	 * @see #getLTS()
	 * @generated
	 */
	EReference getLTS_Alphabet();

	/**
	 * Returns the meta object for the reference '{@link de.luh.se.fmse.lts.LTS#getStartState <em>Start State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Start State</em>'.
	 * @see de.luh.se.fmse.lts.LTS#getStartState()
	 * @see #getLTS()
	 * @generated
	 */
	EReference getLTS_StartState();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.lts.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see de.luh.se.fmse.lts.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for the attribute '{@link de.luh.se.fmse.lts.State#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.luh.se.fmse.lts.State#getName()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_Name();

	/**
	 * Returns the meta object for the reference list '{@link de.luh.se.fmse.lts.State#getIncomingTransitions <em>Incoming Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming Transitions</em>'.
	 * @see de.luh.se.fmse.lts.State#getIncomingTransitions()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_IncomingTransitions();

	/**
	 * Returns the meta object for the reference list '{@link de.luh.se.fmse.lts.State#getOutgoingTransitions <em>Outgoing Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outgoing Transitions</em>'.
	 * @see de.luh.se.fmse.lts.State#getOutgoingTransitions()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_OutgoingTransitions();

	/**
	 * Returns the meta object for the attribute list '{@link de.luh.se.fmse.lts.State#getAtomicPrep <em>Atomic Prep</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Atomic Prep</em>'.
	 * @see de.luh.se.fmse.lts.State#getAtomicPrep()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_AtomicPrep();

	/**
	 * Returns the meta object for the attribute list '{@link de.luh.se.fmse.lts.State#getSatisfiedNodes <em>Satisfied Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Satisfied Nodes</em>'.
	 * @see de.luh.se.fmse.lts.State#getSatisfiedNodes()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_SatisfiedNodes();

	/**
	 * Returns the meta object for the attribute '{@link de.luh.se.fmse.lts.State#isSatisfied <em>Satisfied</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Satisfied</em>'.
	 * @see de.luh.se.fmse.lts.State#isSatisfied()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_Satisfied();

	/**
	 * Returns the meta object for the attribute '{@link de.luh.se.fmse.lts.State#isLTSsatisfied <em>LT Ssatisfied</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>LT Ssatisfied</em>'.
	 * @see de.luh.se.fmse.lts.State#isLTSsatisfied()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_LTSsatisfied();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.lts.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see de.luh.se.fmse.lts.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the attribute '{@link de.luh.se.fmse.lts.Transition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.luh.se.fmse.lts.Transition#getName()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Name();

	/**
	 * Returns the meta object for the reference '{@link de.luh.se.fmse.lts.Transition#getFromState <em>From State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From State</em>'.
	 * @see de.luh.se.fmse.lts.Transition#getFromState()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_FromState();

	/**
	 * Returns the meta object for the reference '{@link de.luh.se.fmse.lts.Transition#getToState <em>To State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To State</em>'.
	 * @see de.luh.se.fmse.lts.Transition#getToState()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_ToState();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.lts.Alphabet <em>Alphabet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alphabet</em>'.
	 * @see de.luh.se.fmse.lts.Alphabet
	 * @generated
	 */
	EClass getAlphabet();

	/**
	 * Returns the meta object for the attribute list '{@link de.luh.se.fmse.lts.Alphabet#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Elements</em>'.
	 * @see de.luh.se.fmse.lts.Alphabet#getElements()
	 * @see #getAlphabet()
	 * @generated
	 */
	EAttribute getAlphabet_Elements();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	LtsFactory getLtsFactory();

} //LtsPackage
